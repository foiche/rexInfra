package Webserver::Apache;

use Rex -base;

desc 'Installation d’Apache';
task 'install_apache',
  group => 'myservers',
  sub {
    update_package_db
    pkg 'apache2', ensure => 'present';
    service 'apache2', ensure => 'started';
  };

desc 'Configuration d’Apache';
task 'configure_apache',
    group => 'myservers',
  sub {
    file "/var/www/monsite",
      ensure => "directory", 
      owner  => "www-data",
      group  => "www-data",
      mode   => "755";

    file "/var/www/monsite/index.html",
      source => "files/index.html",
      owner  => "www-data",
      group  => "www-data",
      mode   => "755";
  
    file "/etc/apache2/sites-available/000-default.conf",
      ensure => "absent";

    file "/etc/apache2/sites-available/monsite.conf",
      owner   => "root",
      group   => "root",
      mode    => "644",
      content => template(
        "templates/monsite.conf.tpl",
        conf => {
          document_root  => '/var/www/monsite',
          admin     => 'webmaster@localhost',
        }
      );
    
    run 'a2ensite monsite.conf';
    service 'apache2', 'reload';
  };

desc 'add_crontab';
task "add_cron", 
  group => 'myservers',
  sub {
    cron add => "root", {
      minute => '5',
      hour  => '*',
      day_of_month   => '*',
      month => '*',
      day_of_week => '*',
      command => '/usr/local/bin/apache_access_log.sh',
     };
  };

1;

=pod

=head1 NAME

$::module_name - {{ SHORT DESCRIPTION }}

=head1 DESCRIPTION

{{ LONG DESCRIPTION }}

=head1 USAGE

{{ USAGE DESCRIPTION }}

 include qw/Webserver::Apache/;

 task yourtask => sub {
    Webserver::Apache::example();
 };

=head1 TASKS

=over 4

=item example

This is an example Task. This task just output's the uptime of the system.

=back

=cut