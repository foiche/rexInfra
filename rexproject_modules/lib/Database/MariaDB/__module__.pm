package Database::MariaDB;

use Rex -base;

task 'install_mariaDB',
  group => 'myservers',
  sub {
    pkg 'mariadb-server', ensure => 'present'; 
  };


1;

=pod

=head1 NAME

$::module_name - {{ SHORT DESCRIPTION }}

=head1 DESCRIPTION

{{ LONG DESCRIPTION }}

=head1 USAGE

{{ USAGE DESCRIPTION }}

 include qw/Database::MariaDB/;

 task yourtask => sub {
    Database::MariaDB::example();
 };

=head1 TASKS

=over 4

=item example

This is an example Task. This task just output's the uptime of the system.

=back

=cut