# Commande mysql bdd rex (rexSlave2)

    create database rex;
    GRANT ALL PRIVILEGES ON rex.* TO 'rex'@'%';
    FLUSH PRIVILEGES;
    use rex;
    create table host(     ip varchar(255) not null,     username varchar(255) not null,     password varchar(255) not null );
    describe host;
    INSERT INTO host (ip, username, password) VALUES ('10.0.0.11', 'rex','rex');
