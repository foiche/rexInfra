# rexInfra

Environnement de test Vagrant pour (R)?ex.
Exemples de codes Rex dans les répertoires "rexproject" et "rexproject-modules"

Un maître = rexMaster
2 esclave = rexSlave1 et 2

## Prérequis
    # apt-update
    # apt-install virtualbox

## Déployer la plateforme de test avec Vagrant

    # vagrant up
    # vagrant ssh rexMaster
    
## Supprimer la plateforme de test

    # vagrant halt (arrêt des VMs)
    # vagrant destroy (suppression des VMs)